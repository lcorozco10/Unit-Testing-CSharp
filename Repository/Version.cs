﻿using System.Configuration;

namespace Repository
{
    public class VersionHelper
    {
        public static string Version()
        {
            string version = System.Reflection.Assembly.GetExecutingAssembly()
                .GetName()
                .Version
                .ToString();
            return $"{version}";
        }

        public static string ValueConfig()
        {
            return ConfigurationManager.AppSettings["Test"];
        }
    }
}

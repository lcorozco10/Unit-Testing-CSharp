﻿using System;
using System.Configuration;
using System.Linq;

namespace Repository
{
    public class Balance
    {
        public static int Sum(string number1, string number2)
        {
            int num1;
            int num2;

            if (number1 == null || number2 == null)
            {
                throw new ArgumentException("Los parametros Null fueron permitidos");
            }

            number1 = number1.Split('.').FirstOrDefault();
            number2 = number2.Split('.').FirstOrDefault();

            if (!int.TryParse(number1, out num1) || !int.TryParse(number2, out num2))
            {
                throw new ArgumentException("No se permite Carateres especiales o letras");
            }

            return num1 + num2;
        }
    }
}

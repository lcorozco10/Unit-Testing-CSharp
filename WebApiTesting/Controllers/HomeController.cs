﻿using System.Web.Mvc;
using Repository;

namespace WebApiTesting.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            ViewBag.Vesion = VersionHelper.Version();
            ViewBag.ValueConfig = VersionHelper.ValueConfig();
            return View();
        }
    }
}
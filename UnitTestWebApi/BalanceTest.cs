﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Repository;

namespace UnitTestWebApi
{
    [TestClass]
    public class SumTest
    {

        [TestMethod]
        public void SumIs4()
        {
            var expect = 4;
            var actual = Balance.Sum("2","2");
            Assert.AreEqual(expect,actual, "La suma nunca es 4");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),"Los numeros no pueden ser Null")]
        public void SumAreParamsNull()
        {
            Balance.Sum(null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Letras y caracters especiales fueron permitidos")]
        public void SumAreParamsSpecialCharacterOrAlphabet()
        {
            Balance.Sum("A", "D");
            Balance.Sum("^", ".");
            Balance.Sum("", "");
            Balance.Sum(" ", " ");
        }

        [TestMethod]
        public void SumAllowDecimalStringParams()
        {
            var expect  = 4;
            var actual = Balance.Sum("2.5", "2.5");
            Assert.AreEqual(expect,actual);
        }
    }
}
